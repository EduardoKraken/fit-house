import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'
import vuetify from '@/plugins/vuetify';

export default{
	namespaced: true,
	state:{
		loginFitHouse:false,
		datosUsuarioFitHouse:'',
	},

	mutations:{
		LOGEADO(state, value){
			state.loginFitHouse = value
		},

		DATOS_USUARIO(state, datosUsuarioFitHouse){
      state.datosUsuarioFitHouse = datosUsuarioFitHouse
		},

		SALIR(state){
			state.loginFitHouse = false
			state.datosUsuarioFitHouse = ''
		},
	},

	actions:{
		guardarInfo({commit, dispatch}, usuario){
			commit('DATOS_USUARIO',usuario)
			commit('LOGEADO',true)
		},

		salirLogin({commit}){
			commit('SALIR')
		},
	},

	getters:{
		getLogeadoFitHouse(state){
		  return state.loginFitHouse
		},

		getdatosUsuarioFitHouse(state){
			return state.datosUsuarioFitHouse
		},

	}
}