import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import es from 'vuetify/es5/locale/es';
import colors from 'vuetify/es5/util/colors'
Vue.use(Vuetify);

export default new Vuetify({
	lang:{
    locales:{ es },
    current: 'es'
  },
  theme: {
    themes: {
      light: {
        primary     : "#ff1313", // #E53935
      },
    },
  },
});