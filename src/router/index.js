import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView  from '../views/HomeView.vue'
import Login     from '../views/Login.vue'

// CATALOGOS
import Categorias     from '../views/catalogos/Categorias.vue'
import Usuarios       from '../views/catalogos/Usuarios.vue'
import Paquetes       from '../views/catalogos/Paquetes.vue'

import Programacion      from '../views/administracion/Programacion.vue'
import MiProgramacion    from '../views/administracion/MiProgramacion.vue'
import HistorialPagos    from '../views/administracion/HistorialPagos.vue'
import Calendarios       from '../views/administracion/Calendarios.vue'
import Registro          from '../views/administracion/Registro.vue'

import Perfil            from '../views/usuarios/Perfil.vue'
import CompraPaquetes    from '../views/usuarios/CompraPaquetes.vue'

import ProcesarPago      from '../views/ProcesarPago.vue'



Vue.use(VueRouter)

const routes = [
  { path: '/'          , name: 'Login'      , component: Login      },
  { path: '/home'      , name: 'Home'       , component: HomeView   },
  { path: '/categorias', name: 'Categorias' , component: Categorias },
  { path: '/usuarios'  , name: 'Usuarios'   , component: Usuarios   },
  { path: '/paquetes'  , name: 'Paquetes'   , component: Paquetes   },

  // Administrador 
  { path: '/programacion'    , name: 'Programacion'    , component: Programacion   },
  { path: '/historialpagos'  , name: 'HistorialPagos'  , component: HistorialPagos },
  { path: '/calendarios'     , name: 'Calendarios'     , component: Calendarios    },
  { path: '/registro'        , name: 'Registro'        , component: Registro       },
  
  { path: '/perfil'          , name: 'Perfil'          , component: Perfil         },
  { path: '/comprapaquetes'  , name: 'CompraPaquetes'  , component: CompraPaquetes },

  { path: '/procesarpago'    , name: 'ProcesarPago'    , component: ProcesarPago   },
  { path: '/miprogramacion'  , name: 'MiProgramacion'  , component: MiProgramacion   },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
