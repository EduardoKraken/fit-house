import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import VueResource from "vue-resource";
import VueYoutube from 'vue-youtube'
// import config from 'dotenv';


// config.config();
Vue.config.productionTip = false;

Vue.use(VueResource);
Vue.use(VueYoutube);

Vue.http.interceptors.push((request, next) => {
  request.headers.set('Authorization', 'Bearer ' + localStorage.tlaKey)
  request.headers.set('Accept', 'application/json')
  request.headers.set('Access-Control-Allow-Origin', '*')
  request.headers.set('Access-Control-Allow-Methods','GET,POST,PUT,PATCH,DELETE');
  request.headers.set('Access-Control-Allow-Methods','Content-Type','Authorization');
  next()
});

// if(process.env.NODE_ENV == 'development'){
  // Vue.http.options.root = 'http://localhost:3001/';
// }else{
  Vue.http.options.root = 'https://sofsolution.club/api-fithouse/';
// }

new Vue({
  router,
  store,
  vuetify,
  render: function (h) { return h(App) }
}).$mount('#app')
