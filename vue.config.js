module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],

  // publicPath: process.env.NODE_ENV === 'production' ? '/fit-house/' : '',
  publicPath: process.env.NODE_ENV === 'production' ? '' : '/',

}